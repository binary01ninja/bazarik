const request = require("request");
const cheerio = require("cheerio");
const chalk = require("chalk");



const siteUrl = "https://www.tgju.org/currency";

function isOdd(num) { return num % 2;}

request(siteUrl, function(error, response, html){
    if (!error && response.statusCode == 200){
        const $ = cheerio.load(html);

        

        currency = [];
        $('th').each((index, element) => {
            var item = $(element).text();
            
            if ((
                item.includes('زمان') ||
                item.includes('بیشترین') ||
                item.includes('کمترین') ||
                item.includes('ارز') ||
                item.includes('جدول') ||
                item.includes('عنوان') ||
                item.includes('قیمت') ||
                item.includes('تغییر')) == false

            ){
                currency.push(item)
        }})
        
        currency.splice(0, 1)
        currency.splice(16, 1)

        values = [];
        changes = [];
        $('.nf').each((index, element) => {
            var item = $(element).text();
            if (isOdd(index) == 0) {
                values.push(item)
            } else {
                var tag = $(element).html()
                if (tag.includes('high') == true ) {
                    changes.push('+' + item)
                } else if (tag.includes('low') == true) {
                    changes.push('-' + item)
                } else {
                    changes.push(item)
                }
                
                
        }})
        
    

        function value(index){return values[index]};;
        function change(index){return changes[index]};

        currency.forEach(element => {
            var_values = value(currency.indexOf(element))
            var_changes = change(currency.indexOf(element))

            console.log(element + ': ', var_values)

            if (var_changes.charAt(0) == '+' ) {
                console.log('تغییرات :' + '\n', chalk.green(var_changes))
            } else if (var_changes.charAt(0) == '-') { 
                console.log('تغییرات :' + '\n', chalk.red(var_changes))
            } else {
                console.log('تغییرات :' + '\n', var_changes)
            }
        });

            
        
   } else{
        console.log('cant fetch the web page')
    }

})
