const request = require("request");
const cheerio = require("cheerio");


const siteUrl = "https://www.tgju.org/";

request(siteUrl, function(error, resp, html) {
        if (!error && resp.statusCode == 200){
            const $ = cheerio.load(html);

            key_array = [];
            $('.info-bar h3').each((index, element) => {
                var item = $(element).text();
                key_array.push(item)
            })
            

            value_array = [];
            $('.info-bar span[class=info-price]').each((index, element) => {
                var item = $(element).text();
                value_array.push(item)

            })
            
            function values(index){return value_array[index]}
            
            key_array.forEach(element => {
                console.log(element + ': ', values(key_array.indexOf(element)))
            });
            

        } else {
          console.log('Cant fetch the web page')
      }
});

